﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class camara : MonoBehaviour
{
    public Camera gameCamera;
    public cosas jugador;
    //Estos dos ints son contadores que se usan para spawnear un bloque dorado o aumentar la dificultad
    private int puntodorado;
    private int dificultad;
    //Aqui estan los prefabs de los bloques
    public GameObject[] bloques;
    //En estos 3 gameobject guardo 3 bloques. El primero es el bloque principal el segundo el bloque dorado y el tercero el checkpoint.
    public GameObject Bloque1;
    public GameObject Bloque2;
    public GameObject Bloque3;
    //Estos dos floats son el puntero y la distanciabloques que va augmentando segun aumenta la dificutlad.
    private float puntero;
    private float distanciabloques = 2f;
    //Estos 2 floats son para el color de background que va cambiando en otro color segun el tiempo. De un color a otro tarda 20 segundos.
    float duracion = 20;
    float tiempo = 0;
    //En este array de colores estan puestos los colores. Y el indexcolor es en el color que esta actualmente.
    public Color[] colores;
    private int indexcolor;
    private Color Color1;

    void Update()
    {
        //Aqui esta cambiando el color del background de uno a otro en el tiempo que son 20 segundos
        gameCamera.backgroundColor= Color.Lerp(Color1, colores[indexcolor], tiempo);
        //Si el tiempo va a llegar a 1 lo pongo a 0 y que se vuelva a repetir
        if (tiempo > .9f)
        {
            tiempo = 0f;
            Color1 = colores[indexcolor];
            indexcolor++;
            indexcolor = (indexcolor >= colores.Length) ? 0 : indexcolor;
        }
        //Si el tiempo es menor que 0 le suma la division de el tiempo que pasado desde el ultimo frame y lo divide entre la duracion
        if (tiempo < 1)
        {
            tiempo += Time.deltaTime / duracion;
        }
        //Esto es la camara siguiendo al jugador
        if (jugador.vivo)
        {
            gameCamera.transform.position = new Vector3(
            jugador.transform.position.x,
            gameCamera.transform.position.y,
            gameCamera.transform.position.z);
        }

        //Aqui esta creando los bloques y aumentando la dificultad , los puntos de los jugadores y los puntos para el bloque dorado que sale cuando pasa de los 50 puntos que podria salir en 5 bloques o en 50.Al aumentar la dificultad
        // crea un bloque que es un checkpoint que le da una vida y si muere lo revive en ese bloque pero no empezara a sumar puntos hasta que llega hasta donde estaba antes.
           if(jugador.vivo && this.puntero < jugador.transform.position.x + 5 && puntodorado<=50)
            {
            jugador.puntos++;
            dificultad += 3;
            puntodorado += UnityEngine.Random.Range(1, 10);
            if (dificultad == 33)
            {
                dificultad = 0;
                distanciabloques += 0.5f;
                GenerarCheckPoint();
            }
            else
            {
                GameObject newBlock = Instantiate(bloques[UnityEngine.Random.Range(0, bloques.Length)]);
                if (newBlock.tag == "MegaInvi")
                {
                    //Porque al bloque true porque a veces salia en false y ya nunca se volvia activar.
                    newBlock.SetActive(true);
                }
                if(newBlock.tag == "Bueno")
                {
                    //llama a una funcion para subir el salto y la velocidad del jugador
                    SubirSalto();
                }
                float size = Bloque1.transform.localScale.x;
                newBlock.transform.position = new Vector2(puntero + size / 2, 0);
                puntero += size + distanciabloques;
            }
            //Aqui crea el bloque dorado.
        } else if (jugador.vivo && this.puntero < jugador.transform.position.x + 5 && puntodorado>=50) {
            jugador.puntos = jugador.puntos * 2;
            puntodorado = 0;
            GameObject newBlock = Instantiate(Bloque2);
            float size = Bloque1.transform.localScale.x;
            newBlock.transform.position = new Vector2(puntero + size / 2, 0);
            puntero += size + 2;
            }else if (jugador.vivo && this.puntero < jugador.transform.position.x +5 && dificultad == 25)
        {
        }
    }

    private void SubirSalto()
    {
        //Sube el salto del jugador si es menor o igual que 65
    if (jugador.GetComponent<cosas>().saltura >= 65)
        {
        }
        else
        {
            jugador.GetComponent<cosas>().saltura += 2.5f;
            jugador.GetComponent<cosas>().velocidad += 0.1f;
        }

    }

    private void GenerarCheckPoint()
    {
        //crea el checkpoint
        GameObject newBlock = Instantiate(Bloque3);
        float size = Bloque1.transform.localScale.x;
        newBlock.transform.position = new Vector2(puntero + size / 2, 0);
        puntero += size + distanciabloques;
        CheckPoint(newBlock);
    }

    private void CheckPoint(GameObject newBlock)
    {
        //guarda las posiciones del jugador en checkpoint
        jugador.GetComponent<cosas>().vidas++;
        jugador.GetComponent<cosas>().CheckX = newBlock.transform.position.x;
        jugador.GetComponent<cosas>().CheckY = newBlock.transform.position.y;
        jugador.GetComponent<cosas>().CheckZ = newBlock.transform.position.z;
        jugador.GetComponent<cosas>().inverso = false;
    }
    

}
