﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using TMPro.Examples;
using UnityEngine;

public class Puntuacion : MonoBehaviour
{
    private TextMeshProUGUI texto;
    public cosas jugador;
    public static int puntuacion = 0;
    void Start()
    {
        texto = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
         texto.text = "Puntuacion: " +jugador.puntos.ToString();
         puntuacion = jugador.puntos;
    }

}
