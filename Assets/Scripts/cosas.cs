﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class cosas : MonoBehaviour
{
    public bool salto = true;
    public float velocidad = 3.5f;
    public float saltura = 50;
    public bool vivo = true;
    //boolean de inverso si esta en true lso controles estan al reves.
    public bool inverso = false;
    public int puntos = 0;
    public int vidas = 0;

    public float CheckX = 0;
    public float CheckY = 0;
    public float CheckZ = 0;



    // Update is called once per frame
    void Update()
    {
        moverse();
        morirse();
    }

    //Funcion morirse. Si tiene menos de 0 vidas se muere. Y si no reaprece en el checkpoint.
    private void morirse()
    {
        if (this.GetComponent<Rigidbody2D>().position.y < -10)
        {
            if (vidas <= 0)
            {
                GameObject.Destroy(this.gameObject);
                vivo = false;
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }
            else
            {
                vidas--;
                this.GetComponent<Rigidbody2D>().transform.position = new Vector3(CheckX, CheckY+2, CheckZ);
            }
        }
    }

    //Moverse. Se mueve.
    private void moverse()
    {
        if (Input.GetKey("d") && !inverso || Input.GetKey("a") && inverso)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(velocidad, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else if(Input.GetKey("a") && !inverso || Input.GetKey("d") && inverso)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-velocidad, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        if (Input.GetKeyDown("space") && salto)
        {
            salto = false;
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, saltura));
        }
    }
    //Esto no se hace en ningun momento creo solo en el priemr bloque.
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "suelo")
        {
            salto = true;
        }
    }

}
