﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bloques : MonoBehaviour
{
    private GameObject jugador;

    void Start()
    {
        //Aqui esta buscando al GameObject jugador
        jugador = GameObject.Find("Jugador");
        //Esta llamando a la funcion Dorado y MegaInvi cada 3 segundos 
        InvokeRepeating("Dorado", 2.0f,3.0f);
        InvokeRepeating("MegaInvi", 1.0f,3.0f);

    }


    void Dorado()
    {
        //Desactiva el render del gameobject con el tag Dorado y activa segun este
        if (this.gameObject.tag == "Dorado" && gameObject.GetComponent<Renderer>().enabled == true)
        {
            gameObject.GetComponent<Renderer>().enabled = false;
        }
        else if(this.gameObject.tag == "Dorado" && gameObject.GetComponent<Renderer>().enabled == false)
        {
            gameObject.GetComponent<Renderer>().enabled = true;
        }
    }

    void MegaInvi()
    {
        //Desactiva el gameobject con el tag MegaInvi y activa segun este

        if (this.gameObject.tag == "MegaInvi" && gameObject.active)
        {
                gameObject.SetActive(false);   
        }
        else if (this.gameObject.tag == "MegaInvi" && !gameObject.active)
        {
            gameObject.SetActive(true);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Mira si el jugador esta colisionando con un objeto con el tag de Malvado y hace que bloque baje hacia abajo
        if(collision.gameObject.tag == "Jugador" && this.gameObject.tag == "Malvado")
        {
            this.gameObject.transform.position = new Vector3(this.transform.position.x,jugador.transform.position.y-1, this.transform.position.z);
            jugador.GetComponent<cosas>().salto = true;

        }
        //Mira si el jugador esta colisionando con un objeto con el tag de Bueno y no hace mucho aparte de poner las cosas en true y en false segun el inverso
        if (collision.gameObject.tag == "Jugador" && this.gameObject.tag == "Bueno")
        {
            jugador.GetComponent<cosas>().salto = true;
            jugador.GetComponent<cosas>().inverso = false;
        }
        //Mira si el jugador esta colisionando con un objeto con el tag de Malvado1 y invierte los controles

        if (collision.gameObject.tag == "Jugador" && this.gameObject.tag == "Malvado1")
        {
            jugador.GetComponent<cosas>().salto = true;
            jugador.GetComponent<cosas>().inverso = true;

        }
        //Mira si el jugador esta colisionando con un objeto con el tag de Bueno y no hace mucho aparte de poner las cosas en true y en false segun el inverso
        if (collision.gameObject.tag == "Jugador" && this.gameObject.tag == "Bueno1")
        {
            jugador.GetComponent<cosas>().salto = true;
            jugador.GetComponent<cosas>().inverso = false;
        }
        //Estos solo ponen el salto en true lo otro lo hacen en gamecontroler o en este caso en el script de camara
        if (collision.gameObject.tag == "Jugador" && this.gameObject.tag == "Dorado")
        {
           jugador.GetComponent<cosas>().salto = true;
        }
        if (collision.gameObject.tag == "Jugador" && this.gameObject.tag == "MegaInvi")
        {
            jugador.GetComponent<cosas>().salto = true;
        }
        if (collision.gameObject.tag == "Jugador" && this.gameObject.tag == "VidaCheck")
        {
            jugador.GetComponent<cosas>().salto = true; 
        }
    }
}
