﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Vidas : MonoBehaviour
{
    public cosas jugador;
    private TextMeshProUGUI texto2;
    void Start()
    {
        texto2 = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        texto2.text = "Vidas : " + jugador.vidas.ToString();
    }

}