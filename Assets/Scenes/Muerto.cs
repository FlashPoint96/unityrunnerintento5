﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Muerto : MonoBehaviour
{
    private TextMeshProUGUI texto;

    void Start()
    {
        texto = GetComponent<TextMeshProUGUI>();
        texto.text = "Has acabado con una puntuacion de : " +Puntuacion.puntuacion.ToString();
    }
}
