-Jugar : Para jugar es con A y D y para saltar con el espacio.
Los Bloques : Bloque Rojo se va para abajo. Bloque Rosa/Rojo invierte los controles. Bloque Azul se hace invisible. Bloque Dorado aparece de vez en cuando. Bloque Azul Clarito es un checkpoint. Bloque verde claro sube velocidad y salto. Bloque verde flojo no hace nada.

Requisits Addicionals
Augment progressiu de la dificultat : La distancia entre bloque y bloque va augmentando segun vas avanzando.

Escenes extres: Escena del Menu y Escena de muerte.

Accés a components per codi més enllà del RigidBody i el Transform : Creo que si. El renderer y al script del jugador.

Ús correcte de backgrounds, especialment amb tècniques com Parallax : Background con un Color.Lerp que va cambiando de color. No se elegir colores.
